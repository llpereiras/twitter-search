class CreateAuthors < ActiveRecord::Migration[5.0]
  def change
    create_table :authors, options: 'ROW_FORMAT=DYNAMIC' do |t|
      t.string :name
      t.string :alias
      t.string :tw_user_id

      t.timestamps
    end
  end
end
