# README

## Dependências

* Ruby >= 2.3
* Rails >= 5.0
* PhantomJS >= 2.1.1
* Chromedriver (https://christopher.su/2015/selenium-chromedriver-ubuntu/)
* Mysql >= 5.5

## Instalação

### GEMs
```
$ bundle update
```

### Database
```
$ rake db:create
```

### Migrations
```
$ rake db:migrate
```

## Task twitter:search

Para buscar os tweets relacionados à alguma hashtag.

####Ex: busca os tweets da hashtag **#rio2016** desde o dia **05/08/2016** até **06/08/2016**
```
$ rake twitter:search[#rio2016,2016-08-05,2016-08-06]
```


## HTML / JSON

Para consumir o conteúdo armazenado (necessário executar o webserver)
```
$ rails s
```
###### acessar http://localhost:3000

* **HTML** http://localhost:3000/tweets
* **JSON** http://localhost:3000/tweets.json


